# GIL 로 성능을 최대한 발휘하기 어려운 구조이다.
# 이러한 코딩은 IO 가 많은 구간에서 활용도가 높으며
# yield 로 충분히 비슷한 효과를 발휘 할 수 있다.
import random
import threading

results = []


def compute():
    results.append(sum([random.randint(1, 100) for i in range(1000000)]))


workers = [threading.Thread(target=compute) for k in range(8)]

for worker in workers:
    worker.start()

for worker in workers:
    worker.join()

print(" Results %s" % results)
