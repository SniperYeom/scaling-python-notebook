import random
from concurrent import futures


def compute():
    return sum([random.randint(1, 100) for i in range(1000000)])


# 기존 processPool 의 경우 CPU의 갯수로 설정된다.
with futures.ProcessPoolExecutor() as executor:
    future_list = [executor.submit(compute) for _ in range(8)]

results = [f.result() for f in future_list]

print(" Results %s" % results)
