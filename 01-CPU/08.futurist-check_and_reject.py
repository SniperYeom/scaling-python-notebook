import random

import futurist
from futurist import rejection
from futurist import waiters


def compute():
    return sum([random.randint(1, 100) for i in range(1000000)])


# 기존 processPool 의 경우 CPU의 갯수로 설정된다.
with futurist.ThreadPoolExecutor(max_workers=8, check_and_reject=rejection.reject_when_reached(2)) as executor:
    future_list = [executor.submit(compute) for _ in range(20)]
    print(executor.statistics)

results = waiters.wait_for_all(future_list)
print(executor.statistics)

print(" Results %s" % [r.result() for r in results.done])
