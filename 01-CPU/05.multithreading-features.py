import random
from concurrent import futures


def compute():
    return sum([random.randint(1, 100) for i in range(1000000)])


with futures.ThreadPoolExecutor(8) as executor:
    future_list = [executor.submit(compute) for _ in range(8)]

results = [f.result() for f in future_list]

print(" Results %s" % results)
