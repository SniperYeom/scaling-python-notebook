import futurist
from futurist import waiters
import random


def compute():
    return sum([random.randint(1, 100) for i in range(1000000)])


# 기존 processPool 의 경우 CPU의 갯수로 설정된다.
with futurist.ThreadPoolExecutor() as executor:
    future_list = [executor.submit(compute) for _ in range(8)]
    print(executor.statistics)

results = waiters.wait_for_all(future_list)
print(executor.statistics)

print(" Results %s" % [r.result() for r in results.done])
