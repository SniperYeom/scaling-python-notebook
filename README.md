# Scaling Python Programming Notebook

> 이 저장소는 Scaling Python Programming 책을 참고하여 직접 내용 및 코드를 정리한 Repository 이다.

 [도서 링크](https://coupa.ng/bgefe8)
 
## CPU 

### Thread
파이썬은 Thread 는 여러 작업을 동시에 실행하기 좋은 방법이다. 
GIT 으로 성능이 제한 되어 CPU 사용 효율을 좋지 못하다.
네트워크나 파일 처럼 IO 구간이 많이 있을 경우 사용하기 좋다.   

일반적으로 threading 모듈을 사용한다.
 

[GIL 설명](https://medium.com/@mjhans83/python-gil-f940eac0bef9)

### Process
멀티 쓰레딩은 GIL 제약 때문에 완벽한 솔루션이 될 수 없다. 병렬 처리 방식으로는 멀티 프로세스를 사용하는것이 좋다.

multiprocessing 패키지를 사용 한다.


###  Futures 
Python 3.2 에서 도임된 concurrent.futures 모듈을 사용하면 비동기 작업 처리를 쉽게 처리 할 수 있다.


### Summary
멀티 Thread 의 경우 IO 사용이 많은 경우 활용 가능하며 
개인적으로 멀티 thread 보아는  yield 처림 sub-routine 활용 화는것도 나쁘지 않다. 

01-CPU Sample 에서 일반적으로 시용방법을 설명 한다.  